from fastapi.exceptions import HTTPException
from fastapi.param_functions import Query
import spacy
import logging
from json import dumps
from fastapi import FastAPI, Response, status, Path

description = """
ML-Web-API is a web API application that uses an NLP model


## Developers:
    - Arif Omar
    - Kouhou Mohamed
    - Lebdaoui Selmane

## School: ENSEIRB-MATMECA

## Supervised by:
    - Mr. Remi Delmas
    - Mr. Christophe Camicas
"""
logging.info("Loading model..")
nlp = spacy.load("./models")
app = FastAPI(
    title="ML-Web-API",
    description=description,
    version="0.0.1",
    contact={
        "name": "Technical team",
        "email": "slebdaoui@enseirb-matmeca.fr",
    }
)


@app.get("/")
async def root():
    return {"message": "Hello World"}


@app.get("/api/intent")
def intent_inference(
        sentence: str = Query(None, description="Sentence to infer")
        ):
    if sentence is None:
        raise HTTPException(
            status_code=422, detail="You must enter a sentence")
    other_pipes = [pipe for pipe in nlp.pipe_names if pipe != "textcat"]
    with nlp.disable_pipes(*other_pipes):
        inference = nlp(sentence)
    return dumps(inference.cats)


@app.get("/api/intent-supported-languages")
def supported_languages():
    return ['fr-FR']


@app.get("/health", status_code=200)
def health_check_endpoint(response: Response):
    response.status_code = status.HTTP_200_OK
    return response
