## Vos services sont vachement gourmand coté ML. Tu peux me fournir la taille de ton image docker et la limite de RAM à définir sur les environnements stp ?
    -> La taille de l'image docker est 534 MB.
    -> Nous avons réalisé des tests en local pour mésurer la consommation de mémoire de l'application. L'image docker_memory_usage.png montre que 
    l'application utilise 380 MB de RAM. C'est pour cela nous proposons de mettre 512 MB comme limite de RAM.
    -> Il y'a une petite confusion concernant la réponse à cette question, vu que la taille de l'image dépasse 512 MB est ce qu'on a vraiment besoin de plus de RAM ? à prioris non, vu que les statistique produites par `docker container stats` montrent que l'image docker utilise moins de 512MB de RAM.

## Le modèle est viable jusqu’à quel trafic en production ? On souhaite avoir un P99 < 200ms:
    -> D'après des stress tests que nous avons réalisés en utilisant locust (Résultats dans le dossier Graphes) montre que le P99 reste inférieur 200ms pour 
    un RPS inférieur à 200.

## On veut enrichir de la donnée dans la stack data avec ton modèle mais ça rame fort, t'as des idées pour améliorer les perfs ?
    -> Il y'a deux aspècts que nous proposons pour améliorer les performances : 
        * Premier aspect concernent l'application, pour pouvoir traiter plus de données rapidement nous pouvons utiliser des technique pour aléliorer la puissance de calcul: parallélisation des traitements (multithreading), utilisation d'autre frameworks pour le serveur (pour remplacer FastAPI par une bibliothèque plus rapide).
        * Deuxième aspect concerne notre réseaux de neurones: S'appuyer sur l'utilisation des GPUs, utilisation des outils d'accélération des résseaux de neurones comme : SpiNNNaker et NNTO.  