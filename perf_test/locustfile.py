from locust import HttpUser, task, between
from random import randrange 


file = "phrases.txt"
file = open(file, 'r')
Lines = file.readlines()
Lines = [u.replace('\n','')  for u in Lines]
nb_lines = len(Lines)


class APIUser(HttpUser):
    host = 'http://ml-web-api.herokuapp.com/'
    wait_time = between(2, 5)
    @task()
    def predict_sent(self):
        self.client.get('api/intent?sentence="' + Lines[ randrange(nb_lines)] + '"')
    @task()
    def supported_languages(self):
        self.client.get('api/intent-supported-languages')