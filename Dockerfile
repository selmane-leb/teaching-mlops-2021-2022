
# Dockerfile

# pull the official docker image
FROM python:3.8.2-slim-buster

# set work directory
WORKDIR /projet

# copy project
COPY . .

RUN pip install -r requirements.txt
RUN pip install fastapi uvicorn

CMD ["gunicorn", "main:app", "--preload", "--worker-class", "uvicorn.workers.UvicornWorker"]
