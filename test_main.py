from main import app
from fastapi.testclient import TestClient


client = TestClient(app)


def test_read_main():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World"}


def test_supported_languages():
    response = client.get("/api/intent-supported-languages")
    assert response.status_code == 200
    assert response.json() == ["fr-FR"]


def test_health_check_endpoint():
    response = client.get("/health")
    assert response.status_code == 200


def test_intent_inference():
    response = client.get("/api/intent")
    assert response.status_code == 422
    response = client.get("/api/intent?sentene=coffee")
    assert response.status_code == 422
    response = client.get("/api/intent?sentence=coffee")
    assert response.status_code == 200
    assert 'purchase' in response.json() and "find-flight" in response.json()


"""
if __name__ == '__main__':
    done = " \033[92mDone\033[0m"

    Tests = [test_read_main,test_supported_languages,
    test_health_check_endpoint,test_intent_inference]

    for i in range(len(Tests)):
        Tests[i]()
        print("Test"+ str(i) +"..."+done)
"""
